
# Services Web - RESTful API
## Laravel 5.7
### Maintenu par Atomrace.com

## Installation et configuration

- git clone git@gitlab.com:atomrace/tuto-laravel-rest-api.git
- cp .env.example .env
- docker-compose up
- docker ps
- docker exec -it laravel_app_1 bash
  -   ls
  -   composer install
  -   php artisan key:generate
  -   php artisan migrate:fresh
  -   php artisan passport:install
  -   php artisan route:list
  -   php artisan l5-swagger:publish
  -   php artisan l5-swagger:generate
  -   composer test
  -   php artisan inspire
  -   php artisan migrate:fresh --seed
  -   php vendor/phpunit/phpunit/phpunit --filter testPostMeasureTest
    
Ouvrir le navigateur aux adresses suivantes : 
- http://localhost/
- http://localhost/api/stations

Ajouter le plugin JSON Formatter à votre navigateur
- https://chrome.google.com/webstore/detail/json-formatter/bcjindcccaagfpapjjmafapmmgkkhgoa?hl=fr

Ouvrir le navigateur à l'adresse suivante : 
- http://localhost/api/stations

Installer Postman et lancer un GET sur
- http://localhost/api/stations

Configurer votre fichier hosts
- sudo nano /etc/hosts

Pour Windows
- notepad %SystemRoot%\System32\drivers\etc\hosts

Ajouter
- 127.0.0.1       restapi.test
- 127.0.0.1       restapi.dev

## Adresses utiles pour le développement
- http://restapi.test/
- http://restapi.test/api/stations
- http://restapi.test/api/documentation
- http://restapi.test/telescope

# Outils de débogage

## Telescope - déjà installé
    Telescope requires Laravel 5.7.7+.

    composer require laravel/telescope
    php artisan telescope:install
    php artisan migrate
    php artisan telescope:publish
    http://localhost/telescope

    Tuto : https://www.youtube.com/watch?v=ctU0FWbutHs
    Bug : https://github.com/laravel/telescope/issues/347


## ClockWork
    https://chrome.google.com/webstore/detail/clockwork/dmggabnehkmmfmdffgajcflpdjlnoemp?hl=fr

    http://localhost/__clockwork/app

    App.php
        'debug' => env('APP_DEBUG', true),

## dump
        $stations = Station::paginate(5);
        dump($stations);

## Log
    https://laravel.com/docs/5.7/logging
    use Illuminate\Support\Facades\Log;

    $message = 'An informational message.';
    Log::debug($message);
    Log::emergency($message);
    Log::alert($message);
    Log::critical($message);
    Log::error($message);
    Log::warning($message);
    Log::notice($message);
    Log::info($message);
    Log::debug($message);

## dump-server
    $ php artisan dump-server
    https://laravel-news.com/laravel-dump-server-laravel-5-7

## Déployer votre application sur Heroku

- Tutoriel : https://atomrace.com/deployer-votre-projet-laravel-5-5-sur-heroku/

  -   git push heroku master
  -   heroku config:set APP_DEBUG=true
  -   heroku config:set APP_LOG_LEVEL=debug

 ### Configuration de la BD 
  -   heroku config:set DB_CONNECTION="pgsql"
  -   heroku config:set DB_HOST="ec2-...compute-1.amazonaws.com"
  -   heroku config:set DB_DATABASE="d382...v3k3p"
  -   heroku config:set DB_USERNAME="zknd...kjoiugmv"
  -   heroku config:set DB_PASSWORD="8841...2e2244f57225d19"
  -   heroku config:set DB_PORT="5432"

# Swagger 
### Configuration de Swagger
  -   heroku config:set L5_SWAGGER_GENERATE_ALWAYS=true
  -   heroku config:set SWAGGER_VERSION=3.0
  -   heroku config:set HTTPS=on
  -   heroku run php artisan l5-swagger:generate

## Documentation Swagger

- Adresse de la doc : http://localhost/api/documentation
- Référence : https://swagger.io/specification/
- Référence annotations : http://zircote.com/swagger-php/

### Installation de Swagger - déjà installé
- composer require "darkaonline/l5-swagger:5.7.*"
- composer require 'zircote/swagger-php:3.*'
- php artisan vendor:publish --provider "L5Swagger\L5SwaggerServiceProvider"
- php artisan l5-swagger:generate

- http://localhost/api/documentation

# Lignes de commandes
## Docker-compose
- docker-compose run app composer test
- docker-compose up -d
- docker-compose logs

## Alias
    alias dcomp="docker-compose"

## PHP
    - php --version
    - php -ini

## TODO : configurer xDebug
    docker xdebug phpstorm - YouTube
    https://www.youtube.com/watch?v=diBNzwAqdn0

# Erreurs rencontrées et solutions

- SQLSTATE[42000]: Syntax error or access violation: 1071 Specified key was too long; max key length is 767 bytes (SQL: alter table `users` add unique  
   `users_email_unique`(`email`))
- Solution : https://laravel-news.com/laravel-5-4-key-too-long-error

- SQLSTATE[42S02]: Base table or view not found: 1146 Table 'homestead.oauth_clients'
- Solution : php artisan migrate:fresh

- Key path "file:///var/www/storage/oauth-private.key" does not exist or is not readable
- Solution : php artisan passport:install

- No application encryption key has been specified.
- Solution : php artisan key:generate

- bootstrap/../vendor/autoload.php. Failed to open stream: No such file or directory. The "vendor" folder does not exist.
- Solution : composer install

- RuntimeException : No application encryption key has been specified.

- Solution : 
    - $ php artisan key:generate --show
    - $ heroku config:set APP_KEY=LA_CLE_GENEREE



## À propos de Atomrace.com

Atomrace.com est une plate-forme de partage de tutoriels pour développeurs Web.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
