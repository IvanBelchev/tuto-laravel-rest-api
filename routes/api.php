<?php

use Illuminate\Http\Request;
use App\Http\Requests\UserRegistrationRequest;
use App\Http\Resources\UserResource;
use App\User;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**
 * Route for user registration
 */
Route::post('/register', 'UserController@store');

/**
 * Routes for user profiles
 */
Route::get('/users/{user}/profile','ProfileController@show');
Route::put('/users/{user}/profile','ProfileController@update');
Route::middleware('auth:api')->get('/user/profile', function (Request $request)
{
    return $request->user()->profile;
});

/**
 * Public routes for stations
 */
Route::get('/stations', 'StationController@index');
Route::get('/stations/{station}', 'StationController@showStationById');
Route::get('/users/{user}/stations', 'StationController@showUsersStation');

/**
 * Routes with auth
 */
Route::middleware('auth:api')->post('/stations', 'StationController@store');
Route::middleware('auth:api')->get('/user/stations', function (Request $request)
{
    return $request->user()->stations;
});
Route::middleware('auth:api')->get('/user', function (Request $request)
{
    return $request->user();
});

/**
 * Routes with auth owner middleware
 */
Route::put('/stations/{stations}', 'StationController@update')
    ->middleware(['auth:api', 'owner:stations']);
Route::delete('/stations/{stations}', 'StationController@destroy')
    ->middleware(['auth:api', 'owner:stations']);

/**
 * Routes for measures
 */
Route::get('/stations/{station}/measures', 'MeasureController@show');
Route::get('/stations/{station}/measures/24h', 'MeasureController@get24h');
Route::post('/stations/{station}/measures', 'MeasureController@store')
    ->middleware(['auth:api', 'owner:station']);

/**
 * Route for testing authentication with Postman
*/
Route::get('/create-personal-token', function () {
    $rnd = random_int(0, 1000);
    $user = new App\User();
    $user->name = $rnd.'oauth';
    $user->password =  Hash::make('secret');
    $user->email = $rnd.'oauth@mail.com';
    $user->save();
    $token = $user->createToken('iot')->accessToken;
    echo $token;
});

/**
 * Telescope demo
 */
Route::get('/demotelescope', function(Request $request){
    dump($request);

    $message = 'An informational message.';
    Log::debug($message);
    Log::emergency($message);
    Log::alert($message);
    Log::critical($message);
    Log::error($message);
    Log::warning($message);
    Log::notice($message);
    Log::info($message);
    Log::debug($message);
    // throw new Exception("Exception de test");
});