<?php

namespace App\Http\Controllers;

use App\Profile;
use App\User;
use App\Http\Requests\ProfilePostRequest;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ProfileController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        if($user->profile == null)
        {
            $newProfile = new Profile();
            $newProfile->user_id = $user->id;
            $user->profile = $newProfile;
            $user->profile->save();
        }

        return $user->profile;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function update(ProfilePostRequest $request, User $user)
    {
        if($user->profile == null)
        {
            $newProfile = new Profile();
            $newProfile->user_id = $user->id;
            $user->profile = $newProfile;
        }
        $user->profile->ddn = $request->ddn;
        $user->profile->web_site_url = $request->web_site_url;
        $user->profile->facebook_url = $request->facebook_url;
        $user->profile->linkedin_url = $request->linkedin_url;

        $user->profile->save();
        return response($user->profile, 200); // Unprocessable Entity
    }

}
